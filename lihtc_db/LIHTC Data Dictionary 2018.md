## Source Dictionary

| Variable Name | Variable Definition                                            | Variable Type | Decimal Places  | Value Labels |
|---------------|----------------------------------------------------------------|---------------|-----------------|--------------|
| HUD_ID        | Unique Project Identifier for the Database                     | A  |  |   |
| PROJECT       | Project name                                                   | A  |  |   |
| PROJ_ADD      | Project street address                                         | A  |  |   |
| PROJ_CTY      | Project city                                                   | A  |  |   |
| PROJ_ST       | Project state                                                  | A  |  |   |
| PROJ_ZIP      | Project zip                                                    | A  |  |   |
| STATE_ID      | State-defined Project ID                                       | A  |  |   |
| CONTACT       | Owner or owner's contact                                       | A  |  |   |
| COMPANY       | Name of contact company                                        | A  |  |   |
| CO_ADD        | Contact's business address                                     | A  |  |   |
| CO_CTY        | Contact's city                                                 | A  |  |   |
| CO_ST         | Contact's state                                                | A  |  |   |
| CO_ZIP        | Contact's zip                                                  | A  |  |   |
| CO_TEL        | Contact's telephone                                            | A  |  |   |
| LATITUDE      | Latitude: Degrees Decimal                                      | N  |  |   |
| LONGITUD      | Longitude: Negative Degrees Decimal -- GIS Mapping Convention  | N  |  |   |
| PLACECE       | Census Place Code (1990)                                       | A  |  |   |
| PLACEFP       | FIPS Place Code (2000)                                         | A  |  |   |
| PLACE2010     | FIPS Place Code (2010)                                         | A  |  |   |
| FIPS1990      | Unique 1990 Census Tract ID                                    | A  |  | If county or tract missing, replaced with ‘X’. |
| FIPS2000      | Unique 2000 Census Tract ID                                    | A  |  | If county or tract missing, replaced with ‘X’. |
| FIPS2010      | Unique 2010 Census Tract:  digits 1-2: State FIPS Code  digits 3-5: County FIPS Code  digits 6-11: Census Tract Number (decimal point excluded) | A | | If county or tract missing, replaced with ‘X’. |
| SCATTERED     | Scattered Site Property 2017                                                           | N |  | 1=Yes  2=No |
| RESYND        | Resyndicated Property 2017                                                             | N |  | 1=Yes  2=No |
| ALLOCAMT      | Annual dollar amount of tax credits allocated 2006                                     | N |  |  |
| N_UNITS       | Total number of units as reported by HFA                                               | N |  |  |
| LI_UNITS      | Total number of low income units as reported by HFA                                    | N |  |  |
| N_0BR         | Number of efficiencies                                                                 | N |  |  |
| N_1BR         | Number of 1-bedroom units                                                              | N |  |  |
| N_2BR         | Number of 2-bedroom units                                                              | N |  |  |
| N_3BR         | Number of 3-bedroom units                                                              | N |  |  |
| N_4BR         | Number of 4-bedroom units                                                              | N |  |  |
| INC_CEIL      | Elected rent/income ceiling for low income units                                       | N |  | 1=50% AMGI  2=60% AMGI  3=Not Reported |
| LOW_CEIL      | Units set aside with rents lower than elected rent/income ceiling 2006                 | N |  | 1=Yes  2=No                            |
| CEILUNIT      | Number of units set aside with rents lower than elected rent/income ceiling 2006       | N |  |                                        |
| YR_PIS        | Year placed in service                                                                 | A |  | 1987-2016=year placed in service 8888=placed in service status not confirmed  9999=placed in service status confirmed, but placed in service year missing |
| YR_ALLOC      | Allocation year                                                                        | A |  |             |
| NON_PROF      | Non-profit sponsor                                                                     | N |  | 1=Yes  2=No |
| BASIS         | Increase in eligible basis                                                             | N |  | 1=Yes  2=No |
| BOND          | Tax-exempt bond received                                                               | N |  | 1=Yes  2=No |
| MFF_RA        | HUD Multi-Family financing/rental assistance                                           | N |  | 1=Yes  2=No |
| FMHA_514      | FmHA (RHS) Section 514 loan                                                            | N |  | 1=Yes  2=No |
| FMHA_515      | FmHA (RHS) Section 515 loan                                                            | N |  | 1=Yes  2=No |
| FMHA_538      | FmHA (RHS) Section 538 loan                                                            | N |  | 1=Yes  2=No |
| HOME          | HOME Investment Partnership Program funds 2003                                         | N |  | 1=Yes  2=No |
| HOME_AMT      | Dollar amount of HOME funds 2006                                                       | N |  |             |
| TCAP          | Tax Credit Assistance Program (TCAP) funds 2012                                        | N |  | 1=Yes  2=No |
| TCAP_AMT      | TCAP Amount 2012                                                                       | N |  |             |
| CDBG          | Community Development Block Grant (CDBG) funds 2003                                    | N |  | 1=Yes  2=No |
| CDBG_AMT      | Dollar amount of CDBG funds 2006                                                       | N |  |             |
| HTF           | Housing Trust Fund funds 2018                                                          | N |  | 1=Yes  2=No |
| HTF_AMT       | Dollar amount of Housing Trust Fund funds 2018                                         | N |  |             |
| FHA           | FHA-insured loan 2003 (FHA Loan Numbers 2006)                                          | N |  | 1=Yes  2=No |
| HOPEVI        | Forms part of a HOPEVI development 2003                                                | N |  | 1=Yes  2=No |
| HPVI_AMT      | Dollar amount of HOPEVI funds for development or building costs 2006                   | N |  |             |
| TCEP          | Tax Credit Exchange Program (TCEP) funds 2012                                          | N |  | 1=Yes  2=No |
| TCEP_AMT      | Dollar amount of TCEP funds 2012                                                       | N |  |             |
| RAD           | Housing Trust Fund funds 2018                                                          | N |  | 1=Yes  2=No |
| QOZF          | Qualified Opportunity Zone Fund funds 2018                                             | N |  | 1=Yes  2=No |
| QOZF_AMT      | Dollar amount of Qualified Opportunity Zone Fund funds 2018                            | N |  | |
| RENTASST      | Federal or state project-based rental assistance contract 2006 (HUD Property ID 2012)  | N |  | 1=Federal  2=State  3=Both Federal and State  4=Neither  5=Unknown whether Federal or State |
| TRGT_POP      | Targets a specific population with specialized services or facilities 2003             | N |  | 1=Yes  2=No                             |
| TRGT_FAM      | Targets a specific population – families 2003                                          | A |  | 1=Yes  2=No  0 or blank = Not indicated |
| TRGT_ELD      | Targets a specific population – elderly 2003                                           | A |  | 1=Yes  2=No  0 or blank = Not indicated |
| TRGT_DIS      | Targets a specific population – disabled 2003                                          | A |  | 1=Yes  2=No  0 or blank = Not indicated |
| TRGT_HML      | Targets a specific population – homeless 2003                                          | A |  | 1=Yes  2=No  0 or blank = Not indicated |
| TRGT_OTH      | Targets a specific population – other 2003                                             | A |  | 1=Yes  2=No  0 or blank = Not indicated |
| TRGT_SPC      | Targets a specific population – other as specified 2003                                | A |  |                                         |
| TYPE          | Type of construction                                                                   | N |  | 1=New construction  2=Acquisition and Rehab  3=Both new construction and A/R 4=Existing |
| CREDIT        | Type of credit percentage                                                              | N |  | 1=30 percent present value 2=70 percent present value 3=Both  4=TCEP only               |
| N_UNITSR      | Total number of units or if total units missing or inconsistent, total low-income units| N |  |                                                                                         |
| LI_UNITR      | Total number of low-income units or if total low-income units missing, total units     | N |  |                                                                                         |
| METRO         | Is the census tract metro or non-metro?                                                | N |  | 1=Metro/Non-Central City  2=Metro/Central City  3=Non-Metro                             |
| DDA           | Is the census tract in a difficult development area? (DDA status is based on placed in service year.) | N               |                 | 0=Not in DDA  1=In Metro DDA  2=In Non-Metro DDA  3=In Metro GO Zone DDA 4=In Non-Metro GO Zone DDA |
| QCT           | Is the census tract a qualified census tract?  (For projects placed in service prior to 2003, QCT is based on 1990 Census tract. For projects placed in service since 2003, QCT is based on 2000 Census tract.) | N |  | 1=In a qualified tract  2=Not in a qualified tract |
| NONPROG       | No longer monitored for LIHTC program compliance due to expired use or other reason 2012  (Status of no longer being monitored for the LIHTC Program is indicated for projects as specified by the allocating agency. This does not indicate whether or not a project remains affordable to low income populations.) | A |  | 1=Yes |
| NLM_REASON    | Reason property is no longer monitored for LIHTC 2018                                  |   |  | 1=Completed Extended-Use Period  2=Sale under Qualified Contract 3=Other |
| NLM_SPC       | Reason property is no longer monitored for LIHTC – other as specified 2018             |   |  |  |
| DATANOTE      | Notes about data record changes processed for database update.                         | A |  |  |
| RECORD_STAT   | Record Status compared to previous version of LIHTC database                           | A |  | N=New  U=Updated  X=Existing (Unchanged from previous DB version) |

-  A=Alphanumeric, contains characters and numbers; N=Numeric, contains numbers including decimal points and negative signs.
- HUD_ID - *characters 1-3: Allocating agency code (see table below) digits 4-7: Year placed in service (0000 or 0001 if unknown or missing)  digit 8: Number of years record was added to database after initial request, e.g. 1= 2015 property reported to HUD with 2016 update (0 if property added to DB prior to 2015 update)  digits 9-11: Record number within allocating agency and year placed in service. Beginning with the 2015 database update, digit 8 (see variable definition) was added to HUD_ID.*
- FIPS - *digits 1-2: State FIPS Code  digits 3-5: County FIPS Code  digits 6-11: Census Tract Number (decimal point excluded)*
